import React from 'react'
import { Link } from 'react-router-dom'

import styles from './style.sass'

const Header = (props) => {
    const { logo, content, action } = props
    return (
        <header className={ styles.header }>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-2">
                        <Link to="/app">{ logo }</Link>
                    </div>
                    <div className="col-8 text-center">
                        { content }
                    </div>
                    <div className="col-2">
                        { action }
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header