import React, { Fragment } from 'react'

import Header from '../Global/Header/index.jsx'

import styles from './style.sass'

const Home = () => (
    <Fragment>
        <Header logo="Logo" content="React.js boilerplate" action="Action" />
        <section className="container">
            <h2 className={ styles.red }>React.js Template</h2>
            <p>A little boilerplate that helps starting new React.js projects.</p>
        </section>
        <div>This is our new home.</div>
    </Fragment>
)

export default Home