## React.js Template

### About
-- A small template for faster start-up of your new application

### Features

```
- Sass preinstalled
- Autoprefixer configured
- Webpack 4
- Babel 7.2.2
- Bootstrap 4
```

### Installation
> *$* npm install / yarn

### Start local dev server
> *$* npm run dev / yarn dev

### Build production version
> *$* npm run prod / yarn prod

